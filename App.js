import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { TabNavigator, StackNavigator } from 'react-navigation';
import { Provider } from 'react-redux';
import store from './store';

import WelcomeScreen from './screens/welcomeScreen';
import AuthScreen from './screens/authScreen';
import SelectionScreen from './screens/selectionScreen';
import MapScreen from './screens/mapScreen';
import SideMenuItems from './components/sideMenu';
import MyTasksScreen from './screens/myTasksScreen';
import CompletedTasksScreen from './screens/completedTasksScreen';

export default class App extends React.Component {
  render(){
   const MainNavigator = TabNavigator({
      welcome: { screen: WelcomeScreen },  //welcome: { screen: WelcomeScreen, navigationOptions: { tabBarVisible : false } }, // !!! Hide Tab in Header
      auth: { screen: AuthScreen },
      selection: { screen: SelectionScreen },
      //map: { screen: MapScreen },
     // mytask: { screen: MyTasksScreen },
      map: { screen: SideMenuItems },
     // completetask: { screen: CompletedTasksScreen },
    },
    {
      tabBarPosition: 'bottom',
      tabBarOptions: {
            labelStyle: { fontSize: 12 }
          },
      swipeEnabled: false
    });
   
    return (
      <Provider store={store}>
        <MainNavigator />
      </Provider>            
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});
