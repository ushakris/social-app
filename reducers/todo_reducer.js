import {
  ADD_TODO,
  UPDATE_TASK
} from '../actions/types';

const INITIAL_STATE = {
  results: []
};

export default function(state = [], action) {
	console.log('action=',action);
  switch (action.type) {
    case ADD_TODO:
      return [action.payload, ...state]
    case UPDATE_TASK:
    	return state.map(todo =>
        (todo.friend_id === action.payload.friend_id && todo.task_id === action.payload.task_id) 
          ? {...todo, completed: !todo.completed}
          : todo
    	)
    default:
      return state;
  }
}