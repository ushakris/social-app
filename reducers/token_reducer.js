import {
  USER_TOKEN,
  CLEAR_USER_ID
} from '../actions/types';


export default function(state = [], action) {
	console.log('action=',action);
  switch (action.type) {
    case USER_TOKEN:
      return action.payload
    case CLEAR_USER_ID:
      return [];
    default:
      return state;
  }
}