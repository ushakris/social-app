import { combineReducers } from 'redux';
import auth from './auth_reducer';
import todo from './todo_reducer';
import list from './friendList_reducer';
import token from './token_reducer';

export default combineReducers({
  auth, todo, list, token
});
