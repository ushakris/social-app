import {
  FRIEND_LIST
} from '../actions/types';


export default function(state = [], action) {
	console.log('action=',action);
  switch (action.type) {
    case FRIEND_LIST:
      return [action.payload, ...state]
    default:
      return state;
  }
}