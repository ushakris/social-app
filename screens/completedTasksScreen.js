import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button, Icon } from 'react-native-elements';

import { connect } from 'react-redux';
import * as actions from '../actions';

class CompletedTasksScreen extends React.Component {
   
  renderCompletedTasks() {
    return this.props.tasks.map((task, i) => {
      if(task.friend_id === this.props.token && task.completed){
        return (
          <Text key={i}> {task.task_msg}</Text>
         )
      }      
    })
  }

  renderScore() {
    let count = 0;
    this.props.tasks.map((task, i) => {
      if(task.friend_id === this.props.token && task.completed){
        count = count + 1;
      }
    });
    return (count * 100);

  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text_header}> Completed Tasks </Text>
        <Text style={styles.text_score}> Score: {this.renderScore()}</Text>
        <View>

          
          {this.renderCompletedTasks()}
        
        </View>
        
        
      </View>
    );
  }
}  

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',    
    justifyContent: 'flex-start'
  },

  text_score: {
    color: 'gold',
    textAlign: 'right',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20    
  },

  text_header: {
    color: 'blue',
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
    marginTop: 30
  }

});

function mapStateToProps({ todo, token }) {
  console.log('todo',todo);
  return { 
            tasks: todo, 
            token: token
         };
}

export default connect(mapStateToProps, actions)(CompletedTasksScreen);

//pass in tasks of this login user as props


