import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button, Icon } from 'react-native-elements';

import { connect } from 'react-redux';
import * as actions from '../actions';

class MyTaskScreen extends React.Component {
  
  renderTasks() {
    self = this;
    if(this.props.tasks.length > 0){
      return this.props.tasks.map((task, i) => {
        if(task.friend_id === this.props.token) {
          return (
            <View key={i} style={{flexDirection: 'row', marginTop: 20}}>
              <Text>  {task.task_msg}  </Text>
              <Button 
                title={task.completed ? 'Done' : 'Available'} 
                small
                style={{backgroundColor: 'green'}}
                disabled={task.completed}
                onPress={() => self.props.Update_Task(task.friend_id, task.task_id)}
              />
            </View>
          )
        }
        });
    }      
  }

 
  render() {
    return (
      <View style={styles.container}>

        <Text style={styles.text_header}> ToDo Tasks  </Text>
          {this.renderTasks()}         
       
      </View>
    );
  }
}  

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',    
    justifyContent: 'flex-start',
  },

  text_header: {
    color: 'blue',
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
    marginTop: 30
  }

});


function mapStateToProps({ todo, token }) {
  console.log('todo',todo);
  return { 
            tasks: todo, 
            token: token
         };
}

export default connect(mapStateToProps, actions)(MyTaskScreen);

//pass in tasks of this login user as props


