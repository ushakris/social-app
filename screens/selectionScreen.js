import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button, Icon } from 'react-native-elements';

import { connect } from 'react-redux';
import * as actions from '../actions';


class SelectionScreen extends React.Component {


  renderMap(){
     this.props.Friend_List();
     this.props.navigation.navigate('map');
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>SelectionScreen!</Text>
        <View style={styles.buttonStyle}>
          <Button title='ToDo Tasks' large onPress={() => this.props.navigation.navigate('task')}/>
        </View>
        <View style={styles.buttonStyle}>
          <Button title='Allot A Tasks' large onPress={this.renderMap.bind(this)}/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  buttonStyle: {
    marginTop: 20,
    marginBottom: 20,
    backgroundColor: 'darkblue'
  }

});


export default connect(null, actions)(SelectionScreen);
