import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import _ from 'lodash';

import { connect } from 'react-redux';
import * as actions from '../actions';

import SideMenuItems from '../components/sideMenu';


class AllotedTasksScreen extends React.Component {
  
  renderAllocated() {
    let uniqueTasks = _.uniqBy(this.props.tasks, 'task_id');
    console.log(uniqueTasks);
    return uniqueTasks.map((task, i) => {
      if(task.assignee_id === this.props.token){
        return (
          <Text key={i}> {task.task_msg}</Text>
         )
      }      
    })
  }

  render() {
    return (
      <View style={styles.container}>
        
        <View>
          <Text style={styles.text_header}> Tasks you Alloted </Text>
           {this.renderAllocated()}
        </View>
      </View>
    );
  }
}  

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',    
    justifyContent: 'flex-start',
  },

  text_header: {
    color: 'blue',
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
    marginTop: 30
  }

});


function mapStateToProps({ todo, token }) {
  console.log('todo',todo);
  return { 
            tasks: todo, 
            token: token
         };
}

export default connect(mapStateToProps, actions)(AllotedTasksScreen);

//pass in tasks of this login user as props


