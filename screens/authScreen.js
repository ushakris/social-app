import React, { Component } from 'react';
import { View, Text, AsyncStorage, TextInput } from 'react-native';
import { Button, Icon } from 'react-native-elements';

import { connect } from 'react-redux';
import * as actions from '../actions';

class AuthScreen extends Component {
  state = {
    login_id: ''
  }

  componentDidMount() {
   // this.props.facebookLogin();
   // this.onAuthComplete(this.props);
    
  }

  componentWillReceiveProps(nextProps) {
    this.onAuthComplete(nextProps);
  }

  onAuthComplete(props) {
    if (props.token) {
      this.props.navigation.navigate('map');
    }
  }
  handleSubmit(){
    this.props.storeUser_id(this.state.login_id);
    this.props.navigation.navigate('selection');
  }

  handleLogout(){
    this.props.clearUser_Id();
    this.setState({login_id: ''});
  }

  render() {
    return (
      <View>
        <Text>AuthSCreen</Text>
        
        <TextInput
          style={{height: 40, width:370, borderColor: 'gray', borderWidth: 1, marginLeft: 15, marginTop: 300, marginBottom: 20}}
          placeholder='Enter Your Login Id' onChangeText={(login_id) => this.setState({login_id})}
          value={this.state.login_id}
        />
        
        <View style={{ marginBottom: 20 }}>
          <Button
            small
            title="Login"
            backgroundColor="lightblue"
            onPress={this.handleSubmit.bind(this)}
          /> 
        </View>  

        <View>
          <Button
            small
            title="LogOut"
            backgroundColor="coral"
            onPress={this.handleLogout.bind(this)}
          /> 
        </View>  
      </View>
    );
  }
}

function mapStateToProps({ auth }) {
  return { token: auth.token };
}

export default connect(mapStateToProps, actions)(AuthScreen);
