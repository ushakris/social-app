import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button, Icon } from 'react-native-elements';

import { connect } from 'react-redux';
import * as actions from '../actions';

import _ from 'lodash';

class TaskScreen extends React.Component {
  state = {
    count: 0
  }

  renderTasks() {
    self = this;
    if(this.props.tasks.length > 0){
      return this.props.tasks.map((task, i) => {
        if(task.friend_id === this.props.token) {
          return (
            <View key={i} style={{flexDirection: 'row', marginTop: 20}}>
              <Text>  {task.task_msg}  </Text>
              <Button 
                title={task.completed ? 'Done' : 'Available'} 
                small
                style={{backgroundColor: 'green'}}
                disabled={task.completed}
                onPress={() => self.props.Update_Task(task.friend_id, task.task_id)}
              />
            </View>
          )
        }
        });
    }      
  }

  renderCompletedTasks() {
    return this.props.tasks.map((task, i) => {
      if(task.friend_id === this.props.token && task.completed){
        return (
          <Text key={i}> {task.task_msg}</Text>
         )
      }      
    })
  }

  renderAllocated() {
    let uniqueTasks = _.uniqBy(this.props.tasks, 'task_id');
    console.log(uniqueTasks);
    return uniqueTasks.map((task, i) => {
      if(task.assignee_id === this.props.token){
        return (
          <Text key={i}> {task.task_msg}</Text>
         )
      }      
    })
  }

  renderScore() {
    let count = 0;
    this.props.tasks.map((task, i) => {
      if(task.friend_id === this.props.token && task.completed){
        count = count + 1;
      }
    });
    return <Text> {count * 100}  </Text>

  }

  render() {
    return (
      <View style={styles.container}>

        <View>

          <Text style={styles.text_style}> ToDo Tasks  </Text>
            {this.renderTasks()}         

        </View>
        <View>

          <Text style={styles.text_style}> Completed Tasks </Text>
          {this.renderCompletedTasks()}
        
        </View>
        <View>

          <Text style={styles.text_style}> Your Score: </Text>
           {this.renderScore()}

        </View>

        <View>

          <Text style={styles.text_style}> Tasks you Allocated </Text>
           {this.renderAllocated()}

        </View>
        
      </View>
    );
  }
}  

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',    
    justifyContent: 'flex-start',
  },

  text_style: {
    backgroundColor: 'blue',
    color: 'white',
    textAlign: 'left',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
    marginTop: 10
  }

});


function mapStateToProps({ todo, token }) {
  console.log('todo',todo);
  return { 
            tasks: todo, 
            token: token
         };
}

export default connect(mapStateToProps, actions)(TaskScreen);

//pass in tasks of this login user as props


