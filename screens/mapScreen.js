import React from 'react';
import { StyleSheet, Text, TextInput, View, ActivityIndicator, Platform } from 'react-native';
import { Button, Icon } from 'react-native-elements';
import { MapView } from 'expo';

import { connect } from 'react-redux';
import * as actions from '../actions';

class MapScreen extends React.Component {

  state = {
    mapLoaded: false,
    region: {
      longitude: -122,
      latitude: 37,
      longitudeDelta: 0.04,
      latitudeDelta: 0.09
    },
    text: '',
    task_id: 1
  }

  componentDidMount() {
    this.setState({ mapLoaded: true });
  }

 handleSubmit(){
  this.props.friend_list[0].map((marker, i) => {   
    this.props.Add_Todo(this.state.text, marker.friend_id, this.state.task_id, this.props.token);
  });
  console.log(this.state.task_id);
  console.log("handleSubmit");
  this.setState({task_id: this.state.task_id + 1});
 }

  renderMarkers(){
    if(this.props.friend_list.length > 0) {
      return this.props.friend_list[0].map((marker, i) => {
              return (
                <MapView.Marker
                  key={i}
                  coordinate={marker.coordinate}
                  title={marker.name}
                  description={marker.phone}
                />
              )
            });
    }
    
  }   
 
  render() {

    if (!this.state.mapLoaded) {
      return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <ActivityIndicator size="large" />
        </View>
      );
    }

   return (
        <View style={{ flex: 1 }}>
            <MapView 
              region={this.state.region}             
              style={{ flex: 1 }}            
            > 
            {this.renderMarkers()}

            </MapView>

            <TextInput
              style={{height: 40, width: 370, marginLeft: 15, marginTop: 20, marginBottom: 20, borderColor: 'gray', borderWidth: 1}}
              placeholder='Enter your Task' onChangeText={(text) => this.setState({text})}
              value={this.state.text}
            />
            <View style={{ marginBottom: 10 }}>
              <Button
                small
                title="+"
                backgroundColor="coral"
                onPress={this.handleSubmit.bind(this)}
              /> 
            </View>         
        </View>                       
    );
  }
}

const styles = {
  buttonContainer: {
    position: 'absolute',
    bottom: 20,
    left: 0,
    right: 0
  }
}

const markers = [
    {
      coordinate: {
        latitude: 37,
        longitude: -122,
      },
      title: 'titleOne',
      description: 'descriptionOne'
    },
    
    {
      coordinate: {
        latitude: 37.004394,
        longitude: -122.025796,
      },
      title: 'titleTwo',
      description: 'descriptionTwo'
    },

    {
      coordinate: {
        latitude: 37.010227,
        longitude: -122.043575,
      },
      title: 'titleThree',
      description: 'descriptionThree'
    },

    {
      coordinate: {
        latitude: 36.97763,
        longitude: -122.054306,
      },
      title: 'titleFour',
      description: 'descriptionFour'
    }    
];

function mapStateToProps({ list, token }) {
  console.log('list',list);
  return { 
            friend_list: list,
            token: token
          };
}


export default connect(mapStateToProps, actions)(MapScreen);
