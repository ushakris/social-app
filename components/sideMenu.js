import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { SideMenu, List, ListItem } from 'react-native-elements';

import AllotedTasksScreen from '../screens/allotedTasksScreen';
import MapScreen from '../screens/mapScreen';
import MyTasksScreen from '../screens/myTasksScreen';
import CompletedTasksScreen from '../screens/completedTasksScreen';


export default class SideMenuItems extends React.Component {

    constructor () {
      super()
      this.state = {
        isOpen: false,
        mapFlag: true,
        allotFlag: false,
        mytaskFlag: false,
        completeFalg: false
      }
      this.toggleSideMenu = this.toggleSideMenu.bind(this)
    }

    onSideMenuChange (isOpen: boolean) {
      this.setState({
        isOpen: isOpen
      })
    }

    toggleSideMenu () {
      this.setState({
        isOpen: !this.state.isOpen
      })
    }
    showMap(){
      this.setState({mapFlag: true});
      this.setState({allotFlag: false});
      this.setState({mytaskFlag: false});
      this.setState({completeFalg: false});
      this.toggleSideMenu();
    }

    showAllotTask(){
      this.setState({mapFlag: false});
      this.setState({allotFlag: true});
      this.setState({mytaskFlag: false});
      this.setState({completeFalg: false});
      this.toggleSideMenu();
    }

    showTodoTasks(){
      this.setState({mapFlag: false});
      this.setState({allotFlag: false});
      this.setState({mytaskFlag: true});
      this.setState({completeFalg: false});
      this.toggleSideMenu();
    }

    showCompleteTasks(){
      this.setState({mapFlag: false});
      this.setState({allotFlag: false});
      this.setState({mytaskFlag: false});
      this.setState({completeFalg: true});
      this.toggleSideMenu();
    }

    render () {
      
      const MenuComponent = (
        <View style={{flex: 1, backgroundColor: '#ededed', paddingTop: 50}}>
          <List containerStyle={{marginBottom: 20}}>
            <ListItem
              title='Map'
              onPress={this.showMap.bind(this)}
            />
            <ListItem
              title='Todo Tasks'
              onPress={this.showTodoTasks.bind(this)}
            />
            <ListItem
              onPress={this.showAllotTask.bind(this)}
              title='Alloted Tasks'
            />
            <ListItem
              onPress={this.showCompleteTasks.bind(this)}
              title='Completed Tasks'
            />
           
          
          </List>
        </View>
      )

      return (
        <SideMenu
          isOpen={this.state.isOpen}
          onChange={this.onSideMenuChange.bind(this)}
          menu={MenuComponent}>
          {this.state.mapFlag && 
            <MapScreen toggleSideMenu={this.toggleSideMenu.bind(this)} />
          }
          {this.state.mytaskFlag &&
            <MyTasksScreen toggleSideMenu={this.toggleSideMenu.bind(this)} />
          }
          {this.state.allotFlag && 
            <AllotedTasksScreen toggleSideMenu={this.toggleSideMenu.bind(this)} />
          }
          {this.state.completeFalg &&
            <CompletedTasksScreen toggleSideMenu={this.toggleSideMenu.bind(this)} />
          }
        </SideMenu>
      )
    }
}