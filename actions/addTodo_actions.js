
import {
  ADD_TODO,
  UPDATE_TASK
} from './types';


export const Add_Todo = (text, friend_id, task_id, assignee_id) => {
  
  const data = {
    assignee_id: assignee_id,
    task_id: task_id,
    completed: false,
    friend_id: friend_id,
    task_msg: text
  }

  
  return {
    payload: data,
    type: ADD_TODO
  };
};


export const Update_Task = (friend_id, task_id) => {
  console.log('friend_id=', friend_id);
  data = {
    friend_id: friend_id,
    task_id: task_id
  }
  console.log('update data=', data);
  return {
    payload: data,
    type: UPDATE_TASK
  };
};


