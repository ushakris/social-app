export const FACEBOOK_LOGIN_SUCCESS = 'facebook_login_success';
export const FACEBOOK_LOGIN_FAIL = 'facebook_login_fail';
export const ADD_TODO = 'add_todo';
export const UPDATE_TASK = 'update_task';
export const FRIEND_LIST = 'friend_list';

export const USER_TOKEN = 'user_token';
export const CLEAR_USER_ID = 'clear_user_id';
