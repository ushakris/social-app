import {
  FRIEND_LIST
} from './types';


export const Friend_List = () => {
  
  const data1 = 
    {
      coordinate: {
        latitude: 37,
        longitude: -122,
      },
      name: 'titleOne',
      friend_id: '1234_56789_1',
      phone: '12345678901'
    };

    const data2 =
    
    {
      coordinate: {
        latitude: 37.004394,
        longitude: -122.025796,
      },
      name: 'titleTwo',
      friend_id: '1234_56789_2',
      phone: '12345678902'
    };

    const data3 =
    {
      coordinate: {
        latitude: 37.010227,
        longitude: -122.043575,
      },
      name: 'titleThree',
      friend_id: '1234_56789_3',
      phone: '12345678903'
    };

    const data4 =

    {
      coordinate: {
        latitude: 36.97763,
        longitude: -122.054306,
      },
      name: 'titleFour',
      friend_id: '1234_56789_4',
      phone: '12345678904'
    }    

  
  return {
    payload: [data1, data2, data3, data4],
    type: FRIEND_LIST
  };
};