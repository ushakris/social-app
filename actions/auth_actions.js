import { AsyncStorage } from 'react-native';
import { Facebook } from 'expo';

import axios from 'axios';


import {
  FACEBOOK_LOGIN_SUCCESS,
  FACEBOOK_LOGIN_FAIL,
  FRIEND_LIST
} from './types';

// How to use AsyncStorage:
// AsyncStorage.setItem('fb_token', token);
// AsyncStorage.getItem('fb_token');
const JOB_ROOT_URL = 'https://graph.facebook.com/';

export const facebookLogin = () => async dispatch => {
  let token = await AsyncStorage.getItem('fb_token');

  if (token) {
    // Dispatch an action saying FB login is done
    dispatch({ type: FACEBOOK_LOGIN_SUCCESS, payload: token });
  } else {
    // Start up FB Login process
    doFacebookLogin(dispatch);
  }
};

const doFacebookLogin = async dispatch => {
  console.log('dofacebooklogin');
  let { type, token } = await Facebook.logInWithReadPermissionsAsync('330916887339258', {
    permissions: ['public_profile']
  });
  console.log('type=', type);
  if (type === 'cancel') {
    return dispatch({ type: FACEBOOK_LOGIN_FAIL });
  }

  await AsyncStorage.setItem('fb_token', token);
  dispatch({ type: FACEBOOK_LOGIN_SUCCESS, payload: token });
  //now get a list of friends
 // getFrinedsList();
};

const getFrinedsList = async() => {
  const user_id = '';
  let url = `${JOB_ROOT_URL}${user_id}/friendlists`;
  try {
    let { data } = await axios.get(url);
    console.log("friends list=", data);
    //Store the friend_list
    //Friend_List( data );
  } catch(e) {
   // console.error(e);
  }

}



/*
const Friend_List = () => {
  
  const data = {[
    {
      coordinate: {
        latitude: 37,
        longitude: -122,
      },
      name: 'titleOne',
      friend_id: 'descriptionOne',
      phone: '12345678901'
    },
    
    {
      coordinate: {
        latitude: 37.004394,
        longitude: -122.025796,
      },
      name: 'titleTwo',
      friend_id: 'descriptionTwo',
      phone: '12345678902'
    },

    {
      coordinate: {
        latitude: 37.010227,
        longitude: -122.043575,
      },
      name: 'titleThree',
      friend_id: 'descriptionThree',
      phone: '12345678903'
    },

    {
      coordinate: {
        latitude: 36.97763,
        longitude: -122.054306,
      },
      name: 'titleFour',
      friend_id: 'descriptionFour',
      phone: '12345678904'
    }    
]};
  
  return {
    payload: data,
    type: FRIEND_LIST
  };
};

*/